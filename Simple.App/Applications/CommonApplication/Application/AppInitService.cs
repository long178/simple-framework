﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simple.AdminApplication;

namespace Simple.CommonApplication.Application
{
    [Transient]
    public class AppInitService : IAppInitService
    {
        AdminDbContext adminDb;
        public AppInitService(AdminDbContext adminDb )
        {
            this.adminDb = adminDb;
        }
        public ApiResult SysDbInit()
        {
            var result = ApiResult.Default;
            return result;
        }
    }
}
